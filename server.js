var express  = require('express');
var app = express();
var mongoose = require('mongoose');
var morgan = require('morgan');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');

mongoose.connect('mongodb://localhost:27017/books');

app.use(express.static(__dirname + '/public'));                 // set the static files location /public/img will be /img for users
app.use(morgan('dev'));                                         // log every request to the console
app.use(bodyParser.urlencoded({'extended':'true'}));            // parse application/x-www-form-urlencoded
app.use(bodyParser.json());                                     // parse application/json
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
app.use(methodOverride());

// listen (start app with node server.js) ======================================
app.listen(8080);
console.log("App listening on port 8080");

var Category = mongoose.model('Categories', {
    text:String
});

var Book = mongoose.model('Books', {
    book_id:String,
    title:String,
    short_description:String,
    description:String,
    author:String,
    date:Date,
    category:String
});

app.disable('etag');

app.get('/', function(req, res) {
    res.sendfile('./public/index.html');
});

app.post('/api/login', function(req, res) {
	if( req.body.password != 'PasswordHardcoded532' )
		res.send('Invalid Password');
	else
		res.send('Access Enabled');
});

app.post('/api/create/category', function(req, res) {
	Category.create({
        text : req.body.category,
        done : false
    }, function(err, todo) {
        if (err)
            res.send(err);

        // get and return all the todos after you create another
        Category.find(function(err, todos) {
            if (err)
                res.send(err)
            res.json(todos);
        });
    });
});

app.get('/api/get/categories', function(req, res) {
	Category.find(function(err, categories) {
        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err)
            res.send(err);

        res.json(categories); // return all todos in JSON format
    });
});

app.post('/api/create/book', function(req, res) {
	console.log(req.body.savedcat);
	Book.create({
        book_id : req.body.id,
		title : req.body.title,
		short_description : req.body.short_description,
		description : req.body.description,
		author : req.body.author,
		date : req.body.pub_date,
		category : req.body.savedcat
    }, function(err, book) {
        if (err)
            res.send(err);

        // get and return all the todos after you create another
        Book.find(function(err, books) {
            if (err)
                res.send(err)
            res.json(books);
        });
    });
});

app.get('/api/get/books', function(req, res) {
	Book.find(function(err, books) {
        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err)
            res.send(err);

        res.json(books); // return all todos in JSON format
    });
});

app.get('/api/books/:item_id', function(req, res) {
    Book.find({
    	_id : req.params.item_id
    },function(err, book) {
        if (err)
            res.send(err)
        res.json(book);
    });
});

app.post('/api/books/:item_id', function(req, res) {
    Book.findOneAndUpdate({
        _id : req.params.item_id
    }, 
    req.body.book,
    {upsert:true},
    function(err, todo) {
        if (err)
            res.send(err);

        // get and return all the todos after you create another
        Book.find(function(err, books) {
            if (err)
                res.send(err)
            res.json(books);
        });
    });
});

app.delete('/api/books/:item_id', function(req, res) {
    Book.remove({
        _id : req.params.item_id
    }, function(err, todo) {
        if (err)
            res.send(err);

        // get and return all the todos after you create another
        Book.find(function(err, books) {
            if (err)
                res.send(err)
            res.json(books);
        });
    });
});