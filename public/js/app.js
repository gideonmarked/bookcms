angular
.module('bookCMS',['ngMaterial',"ngRoute",'ngCookies'])
.config(function($mdThemingProvider,$routeProvider) {

  $mdThemingProvider.theme('docs-dark', 'default')
      .primaryPalette('blue')
      .dark();
  $routeProvider
  .when("/", {
    templateUrl : "partials/login.html",
    controller: "FrontCtrl"
  })
  .when("/dashboard", {
    templateUrl : "partials/dashboard.html",
    controller: "DashboardCtrl"
  })
  .when("/add", {
    templateUrl : "partials/add.html",
    controller: "AddCtrl"
  })
  .when("/edit/:id", {
    templateUrl : "partials/edit.html",
    controller: "EditCtrl"
  })
  .otherwise({
    templateUrl : "partials/404.html"
  });
})

.controller('FrontCtrl', function($scope, $http, $location, $mdDialog,$cookieStore) {
	if($cookieStore.get('logged')) {
		$location.path('/dashboard');
	}
	$scope.validatePassword = function (ev) {
		$http({
			  method  : 'POST',
			  url     : '/api/login',
			  data    : $.param({ password : $scope.password }),
			  headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
			 })
			.then(
				function(data) {
					console.log(data.data);
		            if( data.data != 'Access Enabled' ) {
		            	$mdDialog.show(
					      $mdDialog.alert()
					        .parent(angular.element(document.querySelector('#login-container')))
					        .clickOutsideToClose(true)
					        .title('Error')
					        .textContent(data.data)
					        .ariaLabel('Error Dialog')
					        .ok('Alright')
					        .targetEvent(ev)
					    );
		            } else {
		            	$cookieStore.put('logged',true);
		            	$location.path('/dashboard');
		            }
		        },
		        function(data) {
		            console.log('Error: ' + data);
		        }
	        );
    };
})

.controller('DashboardCtrl', function( $location,$scope,$cookieStore, $http, $mdDialog ) {
	if(!$cookieStore.get('logged')) {
		$location.path('/');
	}

	$http({
			  cache: false,
			  method  : 'GET',
			  url     : '/api/get/books'
			 })
			.then(
				function(data) {
		            $scope.books = data.data;
		        },
		        function(data) {
		            console.log('Error: ' + data);
		        }
	        );

	$scope.delete = function (id) {
		console.log(id);
		var confirm = $mdDialog.confirm()
          .title('Delete Confirmation')
          .textContent('Are you sure you want to delete this item?')
          .ariaLabel('Lucky day')
          .targetEvent(id)
          .ok('Please do it!')
          .cancel('No');

          $mdDialog.show(confirm).then(function() {
				$http({
					cache: false,
					method  : 'DELETE',
					url     : '/api/books/' + id
					})
					.then(
					function(data) {
					    $scope.books = data.data;
					},
					function(data) {
					    console.log('Error: ' + data);
					}
				);
		    }, function() {
		      
		    });
	}

	$scope.edit = function (id) {
		$location.path('/edit/' + id);
	}

	$scope.addBook = function (ev) {
		$location.path('/add');
	}

	$scope.logout = function (ev) {
		$cookieStore.remove('logged');
		$location.path('/');
	}
})

.controller('EditCtrl', function( $location,$scope,$cookieStore, $http, $mdDialog, $routeParams, $route ) {
	if(!$cookieStore.get('logged')) {
		$location.path('/');
	}

	$http({
		  cache: false,
		  method  : 'GET',
		  url     : '/api/books/' + $routeParams.id
		 })
		.then(
			function(data) {
				var book = data.data[0];
				book.date = new Date(book.date);
	            $scope.book = book;

	            $http({
					  cache: false,
					  method  : 'GET',
					  url     : '/api/get/categories'
					 })
					.then(
						function(data) {
				            $scope.categories = data.data;
				        },
				        function(data) {
				            console.log('Error: ' + data);
				        }
			        );
	        },
	        function(data) {
	            console.log('Error: ' + data);
	        }
        );

	$scope.save = function (id) {
		console.log('scope book',$scope.book);
		if(
			$scope.book.book_id == '' ||
			$scope.book.title == '' ||
			$scope.book.short_description == '' ||
			$scope.book.description == '' ||
			$scope.book.author == '' ||
			$scope.book.pub_date == '' ||
			$scope.book.category == '' ||
			$scope.book.book_id == undefined ||
			$scope.book.title == undefined ||
			$scope.book.short_description == undefined ||
			$scope.book.description == undefined ||
			$scope.book.author == undefined ||
			$scope.book.date == undefined ||
			$scope.book.category == undefined
			) {
			$mdDialog.show(
		      $mdDialog.alert()
		        .parent(angular.element(document.querySelector('#login-container')))
		        .clickOutsideToClose(true)
		        .title('Error')
		        .textContent('You must fill in the required fields.')
		        .ariaLabel('Error Dialog')
		        .ok('Alright')
		        .targetEvent(id)
		    );
		} else {
			$http({
			  method  : 'POST',
			  url     : '/api/books/' + id,
			  data    : $.param({
			  	book: $scope.book
			  }),
			  headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
			 })
			.then(
				function(data) {
					$mdDialog.show(
				      $mdDialog.alert()
				        .parent(angular.element(document.querySelector('#login-container')))
				        .clickOutsideToClose(true)
				        .title('Success')
				        .textContent('You have successfully saved the book.')
				        .ariaLabel('Success Dialog')
				        .ok('Alright')
				        .targetEvent(id)
				    );
				    $route.reload();
		        },
		        function(data) {
		            console.log('Error: ' + data);
		        }
	        );
		}
	}

	$scope.cancel = function (ev) {
		$location.path('/');
	}

	$scope.logout = function (ev) {
		$cookieStore.remove('logged');
		$location.path('/');
	}
})

.controller('AddCtrl', function( $location,$scope,$cookieStore, $http,$mdDialog, $route ) {
	$scope.categories = [];

	$http({
			  cache: false,
			  method  : 'GET',
			  url     : '/api/get/categories'
			 })
			.then(
				function(data) {
		            $scope.categories = data.data;
		        },
		        function(data) {
		            console.log('Error: ' + data);
		        }
	        );


	if(!$cookieStore.get('logged')) {
		$location.path('/');
	}

	$scope.addBook = function (ev) {
		console.log('category',$scope.category);
		if(
			$scope.id == '' ||
			$scope.title == '' ||
			$scope.short_desc == '' ||
			$scope.description == '' ||
			$scope.author == '' ||
			$scope.pub_date == '' ||
			$scope.category == '' ||
			$scope.id == undefined ||
			$scope.title == undefined ||
			$scope.short_description == undefined ||
			$scope.description == undefined ||
			$scope.author == undefined ||
			$scope.pub_date == undefined ||
			$scope.category == undefined
			) {
			$mdDialog.show(
		      $mdDialog.alert()
		        .parent(angular.element(document.querySelector('#login-container')))
		        .clickOutsideToClose(true)
		        .title('Error')
		        .textContent('You must fill in the required fields.')
		        .ariaLabel('Error Dialog')
		        .ok('Alright')
		        .targetEvent(ev)
		    );
		} else {
			$http({
			  method  : 'POST',
			  url     : '/api/create/book',
			  data    : $.param({
			  	id : $scope.id,
				title : $scope.title,
				short_description : $scope.short_description,
				description : $scope.description,
				author : $scope.author,
				pub_date : $scope.pub_date,
				savedcat : $scope.category
			  }),
			  headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
			 })
			.then(
				function(data) {
					$mdDialog.show(
				      $mdDialog.alert()
				        .parent(angular.element(document.querySelector('#login-container')))
				        .clickOutsideToClose(true)
				        .title('Success')
				        .textContent('You have successfully added a book.')
				        .ariaLabel('Success Dialog')
				        .ok('Alright')
				        .targetEvent(ev)
				    );
				    $route.reload();
		        },
		        function(data) {
		            console.log('Error: ' + data);
		        }
	        );
		}



		

		
	}

	$scope.addCategory = function (ev) {
		$http({
			  method  : 'POST',
			  url     : '/api/create/category',
			  data    : $.param({ category : $scope.new_category }),
			  headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
			 })
			.then(
				function(data) {
					$scope.categories = data.data;
					$mdDialog.show(
					      $mdDialog.alert()
					        .parent(angular.element(document.querySelector('#login-container')))
					        .clickOutsideToClose(true)
					        .title('Success')
					        .textContent('Category Added Successfully')
					        .ariaLabel('Error Dialog')
					        .ok('Alright')
					        .targetEvent(ev)
					    );
		        },
		        function(data) {
		            console.log('Error: ' + data);
		        }
	        );
	    $scope.new_category = '';
	}

	$scope.cancel = function (ev) {
		$location.path('/');
	}

	$scope.logout = function (ev) {
		$cookieStore.remove('logged');
		$location.path('/');
	}
});